ECOM6001 - Fundamental Business Statistics
==========================================

This repository contains Jupyter Notebooks in association with the teaching of ECOM6001 Fundamental Business Statistics at Curtin University, Western Australia. The notebooks are publicly available but without any guarantee.

The purposes of these notebooks are twofolded. It aims to demonstrate simple statistical ideas via computer simulation with a focus in using Python. As such, it is also the objective of these notebooks to provide a gentle introduction to Python programming with a focus in computation. 

Any question or queries, please contact Felix Chan at fmfchan@gmail.com



